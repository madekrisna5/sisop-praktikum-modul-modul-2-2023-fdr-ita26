# Sisop Modul 2 <br> Kelompok ITA 26



## Soal 1

### Cara Kerja
Program ini merupakan contoh program sederhana untuk mengunduh, mengekstrak, memindahkan, dan mengompres file secara otomatis menggunakan bahasa pemrograman C. Program ini terdiri dari beberapa fungsi yang digunakan untuk melakukan tugas-tugas tertentu seperti:

Fungsi "download_file" untuk mengunduh file dari URL dan menyimpannya dengan nama file yang ditentukan.

Fungsi "unzip" untuk mengekstrak file zip.

Fungsi "create_directory" untuk membuat direktori baru.

Fungsi "move_file" untuk memindahkan file ke direktori tertentu berdasarkan jenis hewan.

Fungsi "zip_directory" untuk mengompres direktori.

Program ini berfungsi untuk mengunduh file zip dari Google Drive yang berisi gambar-gambar hewan. Setelah diunduh, program akan mengekstrak file zip dan memindahkan gambar-gambar tersebut ke direktori tertentu berdasarkan jenis hewan. Kemudian program akan mengompres direktori tertentu untuk menghemat ruang penyimpanan.

Program ini menggunakan beberapa fungsi sistem operasi seperti "system" dan "sprintf" untuk menjalankan perintah shell. Oleh karena itu, program ini hanya dapat dijalankan pada sistem operasi yang mendukung perintah shell seperti Unix/Linux dan MacOS.
### Screenshot File
![Semantic description of image](/Screenshot/File_1.png "file")
### Screenshot Kode
![Semantic description of image](/Screenshot/kode_1_1.png "kode")
![Semantic description of image](/Screenshot/kode_1_2.png "kode")
![Semantic description of image](/Screenshot/kode_1_3.png "kode")
![Semantic description of image](/Screenshot/kode_1_4.png "kode")
### Screenshot Program
![Semantic description of image](/Screenshot/program_1.png "program")

### Kendala yang Dialami

_Tidak ada kendala dalam mengerjakan soal ini_

## Soal 2

### Cara Kerja
Kode ini adalah program C yang menghasilkan beberapa fungsi untuk membuat folder baru, mengunduh beberapa gambar dari internet, memeriksa apakah folder penuh dengan jumlah gambar yang telah diunduh, mengompresi folder, menghapus folder, dan membuat script untuk membunuh program tersebut. Program ini juga menggunakan beberapa header file dari C standar dan beberapa library seperti dirent.h, sys/stat.h, dan signal.h.

Fungsi main() memeriksa jumlah argumen saat menjalankan program dan kemudian melakukan fork() untuk membuat proses anak baru. Selanjutnya, proses anak akan menjalankan fungsi create_folder() untuk membuat folder baru dan create_images() untuk mengunduh gambar dari internet dan menyimpannya ke dalam folder. Jika jumlah gambar dalam folder mencapai batas yang telah ditentukan (dalam hal ini 15), proses anak akan keluar. Proses induk akan terus berjalan dan menunggu proses anak selesai. Setelah proses anak selesai, proses induk akan menjalankan fungsi zip_folder() untuk mengompresi folder yang berisi gambar yang diunduh. Terakhir, program akan menjalankan fungsi create_killer() untuk membuat script shell yang dapat membunuh program dan menghapus script shell tersebut. Script shell ini dapat digunakan dalam beberapa mode berbeda, yang ditentukan oleh argumen saat menjalankan program.

### Screenshot File
![Semantic description of image](/Screenshot/file_2.png "file")
### Screenshot Kode
![Semantic description of image](/Screenshot/kode_2_1.png "kode")
![Semantic description of image](/Screenshot/kode_2_2.png "kode")
![Semantic description of image](/Screenshot/kode_2_3.png "kode")

### Screenshot Program
![Semantic description of image](/Screenshot/program_2.png "program")

### Kendala yang Dialami

- Ketika mendemo, terjadi error ketika compile program

## Soal 3

### Cara Kerja
Program ini adalah program untuk mengkategorikan pemain sepak bola ke dalam directory posisi yang tepat berdasarkan nama file dan rating pemain. Program ini terdiri dari beberapa fungsi yaitu downloadPlayers() untuk mendownload database pemain, extractZip() untuk mengekstrak file zip, removeNonManUtdPlayers() untuk menghapus pemain selain dari Manchester United, createPositionDirectories() untuk membuat directory untuk setiap posisi pemain, categorizePlayers() untuk mengkategorikan pemain ke dalam directory posisi yang tepat, calculateTotalRating() untuk membaca formasi dari file dan menghitung total rating pemain yang diperlukan, dan calculatePositionRatings() untuk menghitung total rating pemain yang tersedia dalam setiap posisi.

Program ini menggunakan beberapa library yaitu stdio.h, stdlib.h, string.h, unistd.h, dirent.h, sys/stat.h, dan sys/wait.h. Program ini juga mendefinisikan beberapa konstanta yaitu MAN_UTD untuk Manchester United, KEEPER_DIR untuk directory kiper, DEFENDER_DIR untuk directory bek, MIDFIELDER_DIR untuk directory gelandang, STRIKER_DIR untuk directory penyerang, dan FORMATION_DIR untuk directory formasi.

Program ini dimulai dengan fungsi main() yang memanggil fungsi downloadPlayers(), extractZip(), removeNonManUtdPlayers(), createPositionDirectories(), categorizePlayers(), dan calculateTotalRating(). Setelah itu, program akan menampilkan total rating pemain yang tersedia dalam setiap posisi menggunakan fungsi calculatePositionRatings().

### Screenshot File
![Semantic description of image](/Screenshot/File_3.png "file")
### Screenshot Kode
![Semantic description of image](/Screenshot/kode_3_1.png "kode")
![Semantic description of image](/Screenshot/kode_3_2.png "kode")
![Semantic description of image](/Screenshot/kode_3_3.png "kode")
![Semantic description of image](/Screenshot/kode_3_4.png "kode")
![Semantic description of image](/Screenshot/kode_3_5.png "kode")
![Semantic description of image](/Screenshot/kode_3_6.png "kode")
### Screenshot Program
![Semantic description of image](/Screenshot/program_3.png "program")

### Kendala yang Dialami
- Kesusahan dalam me link program dengan database

## Soal 4

### Cara Kerja
Kode ini adalah sebuah program C yang digunakan untuk menjalankan sebuah script bash pada waktu tertentu. Program ini menerima 5 argumen dari baris perintah: jam, menit, detik, tanda asterisk (*), dan nama file script bash. Program akan menghitung waktu yang tersisa hingga waktu yang ditentukan, kemudian menunggu hingga waktu tersebut tiba. Setelah itu, program akan menjalankan script bash menggunakan fungsi fork() dan execl(). Jika ada kesalahan dalam argumen atau pengeksekusian script bash, program akan mengeluarkan pesan kesalahan dan keluar dengan kode status 1. Jika program berhasil mengeksekusi script bash, program akan keluar dengan kode status 0.
### Screenshot File
![Semantic description of image](/Screenshot/file_4.png "file")
### Screenshot Kode
![Semantic description of image](/Screenshot/kode_4_1.png "kode")
![Semantic description of image](/Screenshot/kode_4_2.png "kode")
### Screenshot Program
![Semantic description of image](/Screenshot/program_4.png "program")
### Kendala yang Dialami

- Ketika mendemo, terjadi error ketika compile program

